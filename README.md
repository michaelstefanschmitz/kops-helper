# KOPS-Helper

A little helper container to allow KOPS with only Master nodes and Elastic IP Assignment. Please not that IAM Rights
are needed to allow Assignment of ElasticIPs. Only for private use as access rights are widely granted. Example IAM roles look like this:

## IAM

```json
{
    "Version": "2012-10-17",
    "Statement": [
          {
              "Effect": "Allow",
              "Action": "elasticloadbalancing:*",
              "Resource": "*"
          },
          {
              "Effect": "Allow",
              "Action": [
                  "ec2:DescribeAccountAttributes",
                  "ec2:DescribeAddresses",
                  "ec2:DescribeInternetGateways",
                  "ec2:DescribeSecurityGroups",
                  "ec2:DescribeSubnets",
                  "ec2:DescribeVpcs",
                  "ec2:DescribeVpcClassicLink",
                  "ec2:DescribeInstances",
                  "ec2:DescribeNetworkInterfaces",
                  "ec2:DescribeClassicLinkInstances",
                  "ec2:DescribeRouteTables",
                  "cognito-idp:DescribeUserPoolClient"
              ],
              "Resource": "*"
          },
          {
              "Effect": "Allow",
              "Action": "iam:CreateServiceLinkedRole",
              "Resource": "*",
              "Condition": {
                  "StringEquals": {
                      "iam:AWSServiceName": "elasticloadbalancing.amazonaws.com"
                  }
              }
          },
          {
              "Effect": "Allow",
              "Action": [
                  "ec2:AttachNetworkInterface",
                  "ec2:DescribeInstanceStatus",
                  "ec2:DetachNetworkInterface",
                  "ec2:DescribeNetworkInterfaceAttribute",
                  "ec2:ModifyInstanceCreditSpecification",
                  "ec2:AssociateAddress"
              ],
              "Resource": "*"
          }
      ]

}

```
## RBAC K8S

For the untainting an Admin ServiceAccount is easiest way to accomplish the goal:

```yaml
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: k8s-101-role
subjects:
- kind: ServiceAccount
  name: k8s-101-role
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: k8s-101-role
```

# Complete example

```yaml
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: k8s-101-role
subjects:
- kind: ServiceAccount
  name: k8s-101-role
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: k8s-101-role
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kops-helper
  labels:
    app: kops-helper
spec:
  strategy:
    type: Recreate
  replicas: 1
  selector:
    matchLabels:
      app: kops-helper
  template:
    metadata:
      name: kops-helper
      labels:
        app: kops-helper
    spec:
      hostNetwork: true
      hostPID: true
      serviceAccountName: k8s-101-role
      containers:
      - name: kops-helper
        image: registry.gitlab.com/michaelstefanschmitz/kops-helper:2.0.7
        imagePullPolicy: IfNotPresent
        securityContext:
          privileged: true
        env:
        # Name of Elastic IPs to associate
        - name: ELASTIC_IP_NAME
          value: EIP1
        # Check if Master taint is present, remove if "value == true"
        - name: K8S_MASTER
          value: "true"
        # Name of ELBs to associate, have to be unique names
        - name: ELB_LIST
          value: "elb1,elb2"
        # Enable / Disable T2/T3 unlimited
        - name: EC2CREDITLIMIT
          value: "true"
        # If container is used as init container set value to false to block sleep, otherwise optional setting
        - name: SLEEP_INFITY
          value: "true"
```
