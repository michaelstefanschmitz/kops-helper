#!/bin/bash
AWS_DEFAULT_REGION=$(curl -s -m 5 http://169.254.169.254/latest/dynamic/instance-identity/document | jq .region -r)
INSTANCE_ID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)
INSTANCE_NAME=$(curl -s http://169.254.169.254/latest/meta-data/hostname)
ELASTIC_IP_ID=${ELASTIC_IP_NAME:-}
MAXWAIT=3
ALLOC_ID=`aws ec2 describe-addresses --filters="Name=tag:Name,Values=$ELASTIC_IP_ID" | jq -r '.Addresses[] | "\(.InstanceId) \(.AllocationId)"' | grep null | awk '{print $2}' | xargs shuf -n1 -e`
K8S_MASTER=${K8S_MASTER:-}
EC2CREDITLIMIT=${EC2CREDITLIMIT:-}
ELB_LIST=${ELB_LIST:-}
SLEEP_INFITY=${SLEEP_INFITY:=true}

Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Purple='\033[0;35m'       # Purple
Blue='\033[0;34m'         # Blue
NC='\033[0m' # No Color

if [ -z "$ELASTIC_IP_NAME" ]; then
    echo -e "${Blue} No Elastic IP Name set ${NC}"
else
    # Make sure the EIP is free
    echo -e "${Green} Checking if EIP with ALLOC_ID[$ALLOC_ID] is free....${NC}"
    ISFREE=$(aws ec2 describe-addresses --allocation-ids $ALLOC_ID --query Addresses[].InstanceId --output text)
    STARTWAIT=$(date +%s)
    while [ ! -z "$ISFREE" ]; do
        if [ "$(($(date +%s) - $STARTWAIT))" -gt $MAXWAIT ]; then
            echo -e "${Red} WARNING: We waited 30 seconds, we're forcing it now. ${NC}"
            ISFREE=""
        else
            echo -e "${Purple} Waiting for EIP with ALLOC_ID[$ALLOC_ID] to become free....${NC}"
            sleep 3
            ISFREE=$(aws ec2 describe-addresses --allocation-ids $ALLOC_ID --query Addresses[].InstanceId --output text)
        fi
    done

    # Now we can associate the address
    echo Running: aws ec2 associate-address --instance-id $INSTANCE_ID --allocation-id $ALLOC_ID --allow-reassociation
    aws ec2 associate-address --instance-id $INSTANCE_ID --allocation-id $ALLOC_ID --allow-reassociation
fi

if [[ "$K8S_MASTER" = "true" ]]; then
    set -e
    kubectl get nodes -o custom-columns=NAME:.metadata.name,TAINTS:.spec.taints --no-headers
    kubectl taint node $INSTANCE_NAME node-role.kubernetes.io/master- 2>/dev/null || true
    echo -e "${Green} I did my best on removing master taint! ${NC}"
else
    echo -e "${Blue} No Kubernetes Master taint removal ${NC}"
fi

if [[ "$EC2CREDITLIMIT" = "true" ]]; then
    echo -e "${Green} Modifying ec2 unlimited mode on Instance [$INSTANCE_ID] ${NC}"
    aws ec2 modify-instance-credit-specification --instance-credit-specification "InstanceId=$INSTANCE_ID,CpuCredits=standard"
else
    echo -e "${Blue} Not setting T2/T3 limits ${NC}"
fi

if [ -z "$ELB_LIST" ]; then
    echo -e "${Blue} No ELB List set ${NC}"
else
  for i in $(echo $ELB_LIST | sed "s/,/ /g")
  do
    echo -e "${Green} Registering Instances with the ELB [$i] ${NC}"
    aws elb register-instances-with-load-balancer --load-balancer-name $i --instances $INSTANCE_ID
  done
fi

echo -e "${Blue} I have done my job! ${NC}"

if [[ "$SLEEP_INFITY" = "true" ]]; then
  echo -e "${Blue} I'm resting in peace, chill out! ${NC}"
  sleep infinity
fi
