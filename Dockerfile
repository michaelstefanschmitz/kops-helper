FROM    debian:buster-slim

ENV     TERM=xterm \
        DEBIAN_FRONTEND=noninteractive \
        TZ='Europe/Berlin' \
        APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 \
        BASH_ENV='/root/.bashrc'

COPY    .bashrc /root/.bashrc
RUN     chsh -s /bin/bash \
&&      ln -sf /bin/bash /bin/sh \
&&      echo 'deb http://deb.debian.org/debian buster-backports main' > /etc/apt/sources.list.d/backports.list \
&&      echo 'Package: *\nPin: release a=buster-backports\nPin-Priority: 1001' | tee -a /etc/apt/preferences.d/10-backports-pin \
&&      apt update \
&&      apt -y upgrade \
&&      apt -y install --no-install-recommends \
                apt-transport-https \
                ca-certificates \
                curl \
                less \
                jq \
                nano \
                unzip \
#
&&      curl -sL https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o awscliv2.zip \
&&         unzip awscliv2.zip \
&&         aws/install \
&&         rm -rf \
            awscliv2.zip \
            aws \
            /usr/local/aws-cli/v2/*/dist/aws_completer \
            /usr/local/aws-cli/v2/*/dist/awscli/data/ac.index \
            /usr/local/aws-cli/v2/*/dist/awscli/examples \
&&         rm -rf /var/cache/apk/* \
&&      mkdir /root/.aws \
&&	    curl --fail --silent --show-error -O "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" \
&&      chmod +x kubectl \
&&      mv "kubectl" "/usr/local/bin/" \
#
# cleanup apt mess
&&      apt-get clean \
&&      rm -rf \
                /var/lib/apt/lists/* \
                /tmp/* \
                /var/tmp/*

COPY aws-script.sh /usr/bin/aws-script.sh

ENTRYPOINT  ["/usr/bin/aws-script.sh"]
